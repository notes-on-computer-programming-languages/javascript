# javascript

* [*OpenJS Foundation*
  ](https://en.m.wikipedia.org/wiki/OpenJS_Foundation) (WikipediA)

[[_TOC_]]

# Application building tools
## Progressive web app
* [*Progressive web app*](https://en.m.wikipedia.org/wiki/Progressive_web_app)

## Electron
* [*Electron (software framework)*
  ](https://en.m.wikipedia.org/wiki/Electron_(software_framework))
  (WikipediA)

## [Tauri](https://tauri.app)
* [*Tauri_(software_framework)*
  ](https://en.m.wikipedia.org/wiki/Tauri_(software_framework)) (WikipediA)

### Official documentation
* [*What is Tauri?*
  ](https://tauri.app/start/)
* [*Frontend Configuration*
  ](https://tauri.app/start/frontend/)

# Books
* *Secrets of the JavaScript Ninja*
  (**2025**) (Third Edition) Trevor Burnham, Bear Bibeault, Josip Maras, and John Resig (Manning)
---
* *JavaScript Essentials For Dummies*
  **2024**-04 Paul McFedries (For Dummies, Wiley)
* *Build a Frontend Web Framework (From Scratch)*
  **2024**-04 Ángel Sola Orbaiceta (Manning)
* * The Art of Unit Testing, 
with examples in JavaScript*
  **2024**-02 (Third Edition) Roy Osherove with Vladimir Khorikov (Manning)
* [*Eloquent JavaScript*
  ](https://eloquentjavascript.net/)
  **2024** (4th edition) Marijn Haverbeke
* *Generative Art with JavaScript and SVG Utilizing Scalable Vector Graphics and Algorithms for Creative Coding and Design*
  **2024** David Matthew (Apress)
* *JavaScript All-in-One For Dummies*
  **2023**-05 Chris Minnick (For Dummies, Wiley)
* *Mastering JavaScript functional programming : write clean, robust, and maintainable web and server code using functional JavaScript and TypeScript*
  **2023** (Third edition) Federico Kereki
  * 2020 *Mastering JavaScript Functional Programming - Second Edition*
* *Javascript Absolute Beginner's Guide*
  **2022**-11 (3rd Edition) Kirupa Chinnathambi (Que)
* *The Essential Guide to HTML5 Using Games to Learn HTML5 and JavaScript*
  **2022**-10 Jeanine Meyer (Apress)
* *JavaScript from Frontend to Backend*
  **2022** Eric Sarrion
* *Build Your Own 2D Game Engine and Create Great Web Games: Using HTML5, JavaScript, and WebGL2*
  **2021**-12 Kelvin Sung, Jebediah Pavleas, Matthew Munson, and Jason Pace (Apress)
* *Testing JavaScript Applications*
  **2021**-05 Lucas Fernandes da Costa (Manning)
* *The Joy of JavaScript*
  **2021**-03 Luis Atencio (Manning)
* *JavaScript: The Definitive Guide*
  **2020**-05 (7th Edition) David Flanagan (O'Reilly)
* *JavaScript Cookbook*
  **2021**-07 (3rd Edition) Adam D. Scott, Matthew MacDonald, and Shelley Powers (O'Reilly)
* *Get programming With Node.js*
  **2019**  Yoni Wexler (Manning Publications Co.)
* *Functional Programming in JavaScript: How to Improve Your Javascript Programs Using Functional Techniques*
  **2018** Luis Atencio
* *Beginning Functional JavaScript Uncover the Concepts of Functional Programming with EcmaScript 8*
  **2018** Srikanth Machiraju and Anto Aravinth
* *JavaScript for sound artists : learn to code with the Web Audio API*
  **2017**-01 William Turner and Steve Leonard (Focal Press)
* *JavaScript® for kids for dummies®*
  **2015** Chris Minnick and Eva Holland (Hoboken and Wiley)
  * Check new edition (one day)!
* *Web Audio API*
  **2013**-03 Boris Smus (O'Reilly)

## Angular
* See [Typescript](https://gitlab.com/notes-on-computer-programming-languages/typescript/-/blob/main/README.md)

## Backbone.js
* *Full Stack JavaScript Learn Backbone.js, Node.js, and MongoDB*
  **2018** (2nd ed.) Azat Mardan (Apress)

## jQuery and jQuery UI
* *jQuery Recipes: Find Ready-Made Solutions to All Your jQuery Problems*
  **2021**-10 Bintu Harwani (Apress)
* *Mastering jQuery UI*
  **2015**-02 Vijay Joshi (Packt)
* *JavaScript and jQuery for data analysis and visualization*
  **2014**-12 Jon Raasch, Graham Murray, Vadim Ogievetsky, and Joseph Lowery (Wrox)
* *jQuery UI in Action*
  **2014**-09 TJ VanToll (Manning)

## React JS
* *Learning JavaScript Design Patterns a JavaScript and React developer's guide*
  **2023** Addy Osmani (O'Reilly)
* *Beginning react JS foundations building user interfaces with React JS : an approachable guide*
  **2022**-03 Chris Minnick (Wiley)
  * Author of many approachable books
* *ReasonML quick start guide : build fast and type-safe react applications that leverage the JavaScript and OCaml ecosystems*
  **2019** Bruno Joseph D'mello and Raphael Rafatpanah (O'Reilly)

## Svelte
* *Hands-on JavaScript high performance : build faster web apps using Node. js, Svelte. js, and webAssembly*
  **2020** Justin Scherer (Packt)

## Visualization
* *Data Visualization with JavaScript*
  **2015**-03 Stephen A. Thomas (No Starch Press)

## Vue.js
* *Architecting Vue.js 3 Enterprise-Ready Web Applications*
  **2023**-04 Solomon Eseme (Packt)
* *Testing Vue.js Applications*
  **2019**-01 Edd Yerburgh (Manning)
* *Vue.js in Action*
  **2018**-09 Ben Listwon, and Erik Hanchett (Manning)
* *Vue.js: Up and Running*
  **2018**-03 Callum Macrae (O'Reilly Media)
* *Vue.js 2 Design Patterns and Best Practices*
  **2018**-03 Paul Halliday (Packt)

### (fr)
* *Vue.js : développez des applications web modernes en JavaScript avec un framework progressif*
  **2021** Yoann Gauchard (ENI)

# Blogs
* [*Mostly adequate guide to FP (in javascript)*
  ](https://github.com/MostlyAdequate/mostly-adequate-guide)
  (GitHub)
---
* [*Functional Programming with JavaScript*
  ](https://www.telerik.com/blogs/functional-programming-javascript)
  **2021**-07 Leonardo Maldonado

# Language
* [*Here’s How Not to Suck at JavaScript:*
  ](https://betterprogramming.pub/js-reliable-fdea261012ee)
  Let’s be honest. A lot of JavaScript code sucks. Change that!
  **2019**-08 Ilya Suzdalnitski
* [*The Anatomy of a Modern JavaScript Application*
  ](https://www.sitepoint.com/anatomy-of-a-modern-javascript-application/)
  **2018**-04 James Kolce
* *Node.js 8 the Right Way:
  Practical, Server-Side JavaScript That Scales*
  **2017**-12 Jim R. Wilson (The Pragmatic Programmers)

# Implementations
* nodejs (V8)
* mozjs (SpiderMonkey)
* [deno](https://repology.org/project/deno/versions)
  * [Deno (software)](https://en.wikipedia.org/wiki/Deno_(software))
* [bun](https://repology.org/project/bun/versions) (reimplementation of node and npm)
* [pnpm](https://repology.org/project/pnpm/versions) (reimplementation of npm)
  * [node:pnpm](https://repology.org/project/node:pnpm/versions)
  * [pnpm](https://en.wikipedia.org/wiki/Pnpm) (WikipediA)

# Static analysis and languages that compile to JS
* [*List of languages that compile to JS*
  ](https://github.com/jashkenas/coffeescript/wiki/list-of-languages-that-compile-to-js) https://github.com/jashkenas/coffeescript
  Vanished!?
* [*10 Languages That Compile to JavaScript*
  ](https://www.sitepoint.com/10-languages-compile-javascript/)
  **2018**-04 James Kolce
* [What are the best solutions to "The JavaScript Problem"?
  ](https://www.slant.co/topics/1515/~best-solutions-to-the-javascript-problem)
  Slant
* [What are the best languages that compile to JavaScript?
  ](https://www.slant.co/topics/101/~best-languages-that-compile-to-javascript)
  Slant

## Popularity
* ![npm-downloads (recent)](https://badgen.net/npm/dm/typescript)
  [typescript](https://www.npmjs.com/search?q=typescript)
* ![npm-downloads (recent)](https://badgen.net/npm/dm/flow-bin)
  [flow-bin](https://www.npmjs.com/search?q=flow-bin)
  * https://flow.org
  * ![npm-downloads (recent)](https://badgen.net/npm/dm/@babel/preset-flow)
    [@babel/preset-flow](https://www.npmjs.com/search?q=@babel/preset-flow)
  * ![npm-downloads (recent)](https://badgen.net/npm/dm/flow-remove-types)
    [flow-remove-types](https://www.npmjs.com/search?q=flow-remove-types)
* ![npm-downloads (recent)](https://badgen.net/npm/dm/rescript)
  [rescript](https://www.npmjs.com/search?q=rescript)
* ![npm-downloads (recent)](https://badgen.net/npm/dm/purescript)
  [purescript](https://www.npmjs.com/search?q=purescript)
* fable
  * https://fable.io
  * Downloads from .NET
* idris2

# Libraries
## Database libraries
One can make 3 categories:
* SQL helpers,
* query builders (SQL dialect independent and making a base layer for ORMs), and 
* ORMs.

### Connectors
#### Postgres connector
* [node-postgres.com](https://node-postgres.com)
  * [*Environment variables*
    ](https://node-postgres.com/features/connecting)
* [*How to Connect to a PostgreSQL Database Using Node.js*
  ](https://www.makeuseof.com/node-postgresql-connect-how/)
  **2022**-06 Mary Gathoni

### SQL helpers
* Slonik (js)
### Query builders
SQL dialect independent and making a base layer for ORMs
* Knex.js
### ORMs
#### Database: query builder(s) and ORM
* [node js orm](https://google.com/search?q=node+js+orm)
---
* [*Top 11 Node.js ORMs, query builders & database libraries in 2022*
  ](https://www.prisma.io/dataguide/database-tools/top-nodejs-orms-query-builders-and-database-libraries)
  (**2023**-04) Prisma
* [*7 Best and Worst ORM for Node.js in 2023*
  ](https://www.eversql.com/best-orm-for-node-js/)
  **2023**-01 Oded Valin
* [*Node.js ORMs: Why you shouldn’t use them*
  ](https://blog.logrocket.com/node-js-orms-why-shouldnt-use/)
  **2022**-10 Thomas Hunter II (LogRocket)
* [*Battle of the Node.js ORMs: Objection vs. Prisma vs. Sequelize*
  ](https://www.bitovi.com/blog/battle-of-the-node.js-orms-objection-prisma-sequelize)
  **2022**-07 Nauany Costa
* [*Node.js ORMs: Why it's bad for you?*
  ](https://www.atatus.com/blog/node-js-orms-and-why-you-shouldnt-use-them/)
  **2021**-12 (Atatus)
* [*Learn all about Different ORM in Node JS || 2021*
  ](https://medium.com/tkssharma/learn-all-about-different-orm-in-node-js-2021-38536828c1f0)
  **2021**-05 @tkssharma
### Unclassified

## Front-end and interactive web applications
* [*Top 9 Alternatives to ReactJS: Exploring the Best Front-End Frameworks*
  ](https://maybe.works/blogs/alternatives-to-react)
  2024-09 Serhii Holivets (MaybeWorks)

## Functional programming
* [*Exploring DO Notation in JS*
  ](https://dev.to/tracygjg/exploring-do-notation-in-js-5df2)
  **2023**-08 Tracy Gilmore (DEV)

### Contributed libraries
#### Underscore
* https://underscorejs.org

## Web
### Standard library
#### Official documentation
* [*Anatomy of an HTTP Transaction*
  ](https://nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/)

#### Unofficial documentation
* [*Create a backend in Javascript (part 4): Create your first HTTP Server*
  ](https://dev.to/ericchapman/create-a-backend-in-javascript-part-4-create-your-first-http-server-5k1)
  **2021**-10 Eric The Coder
### Contributed libraries
#### List and comparisons
##### Web backend tools
* [*The State of Node.js & JavaScript for Backend Development*
  ](https://snipcart.com/blog/javascript-nodejs-backend-development)
  **2022**-03 Mathieu Dionne
  * Express.js
  * Meteor
  * Sails.js
  * Koa.js
  * Strapi
  * Nest.js, Hapi.js, Socket.io, Mean.js, Total.js, Derby.js & Keystone.js.
* [*How to use Node Js for Backend Web Development in 2022*
  ](https://www.simplilearn.com/tutorials/nodejs-tutorial/nodejs-backend)
  **2022**-02 Simplilearn
  * Nest.js
  * Express.js
  * Socket.io
  * Meteor.js
  * Koa.js
  * Loopback.io
* [*5 Backend JavaScript Frameworks Experts Love*
  ](https://insights.dice.com/2020/08/21/5-backend-javascript-frameworks-experts-love/)
  **2020**-08 Nate Swanner
  * Next.js
  * Express.js
  * Gatsby
  * Node.js
  * Meteor
